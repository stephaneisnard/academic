+++
widget = "experience"
# This file represents a page section.
headless = true
# Order that this section will appear in.
weight = 10

# ... Put Your Section Options Here (title etc.) ...

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.

[[experience]]
  title = "Ingénieur d'étude responsable de Systèmes d'Informations"
  company = "Direction du Système d’Information, Université Toulouse II"
  company_url = ""
  location = "Toulouse"
  date_start = "2010-09-01"
  date_end = "2018-09-01"
  description = """
  * développement d’outils nécessaires au domaine RH
  * mise en œuvre d’un Référentiel transversal
  * urbanisation et cartographie du Système d’Information
  * administration Business Intelligence et création d’univers
  * audit et conseil en sécurité des Développements Web
  * administration d’applications : congés, recrutement EC
  """

[[experience]]
  title = "IGE Responsable du pôle Système d'Information et Services Web"
  company = "Centre de Ressources Informatiques de l'IUT 'A' Paul Sabatier Toulouse III"
  company_url = ""
  location = "Toulouse"
  date_start = "2005-09-01"
  date_end = "2010-09-01"
  description = """
  * participation à la mise en place d'un schéma directeur informatique
  * validation et consolidation du système d’information pour les étudiants
  * conception de Services Web pour l’exploitation d’annuaires OpenLDAP et Active Directory

  Référent ITIL pour les projets:

  * 2009/2010 mise en place d'un service support (équipe de 12 personnes)
  * 2008/2009 rationalisation des méthodes et des procédures (adjoint au chef de service)
  * 2008/2009 rationalisation de l'architecture système et réseau (équipe de 6 personnes)

  Responsable du projet Authentification unique et Nomadisme

  * définition de l'architecture répondant à la qualité et à la continuité de service
  * mise en place d'une solution de synchronisation d'annuaires hétérogènes
  """

[[experience]]
  title = "Ingénieur d'étude Gestionnaire de parc et Développeur d'applications"
  company = "Département informatique IUT TOULOUSE III"
  company_url = ""
  location = "Toulouse"
  date_start = "2003-01-01"
  date_end = "2005-09-01"
  description = """
  * développement d’outils et d'un intranet en PHP/MySQL
  * participation au déploiement logiciel
  * assistance aux utilisateurs et suivi de projets étudiants
  """

[[experience]]
  title = "Contrat Emploi Jeune"
  company = "Etablissement Montalembert Notre Dame"
  company_url = ""
  location = "Toulouse"
  date_start = "1999-01-01"
  date_end = "2003-12-31"
  description = """
  Responsable informatique

* gestion de parc
* administration de serveurs Windows NT4, Linux, maintenance, installation, déploiement

  """
+++
# Enseignant
